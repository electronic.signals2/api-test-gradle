package com.springboot.api.gradle.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.gradle.model.Persona;
import com.springboot.api.gradle.service.impl.PersonaServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping("/persona")
@Api(description = "Api para mantenimiento de persona")
public class PersonaController {
	
	@Autowired
	private PersonaServiceImpl _personaService;
	
	@GetMapping(value = "/all", produces = "application/json")
	@ApiOperation("Lista general de personas")
	public List<Persona> getAllPersonas(){
		return _personaService.getAllPersonas();
	}
	
	@GetMapping(value = "/get/{id}", produces = "application/json")	
	@ApiOperation("Buscar a una persona por ID")
	public Persona getPersona(@ApiParam("Parametro ID") @PathVariable ("id") Integer id){
		return _personaService.getPersona(id);
	}
	
	@PostMapping(value = "/save", produces = "application/json")	
	@ApiOperation("Guardar nueva persona")
	public List<Persona> savePersona(@RequestBody Persona persona){
		
		_personaService.savePersona(persona);
		
		return _personaService.getAllPersonas();
	}	
	
	@DeleteMapping(value = "/delete/{id}", produces = "application/json")	
	public List<Persona> deletePersona(@PathVariable ("id") Integer id){
		
		_personaService.deletePersona(id);
		
		return _personaService.getAllPersonas();
	}	

}
